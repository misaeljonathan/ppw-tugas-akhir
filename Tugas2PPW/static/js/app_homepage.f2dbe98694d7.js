	// Setup an event listener to make an API call once auth is complete
  function onLinkedInLoad() {
      IN.Event.on(IN, "auth", sharedContent);
  }

  // Handle the successful return from the API call
  function onSuccess(data) {
    console.log(data);
  }

  // Handle an error response from the API call
  function onError(error) {
    console.log(error);
  }
  
  function closeSession(){
    IN.User.logout();
  }

  function sharedContent() {
    console.log("Success"+IN.User.isAuthorized());
    IN.API.Raw("/companies?format=json&is-company-admin=true").result(print).error(error403);
  }

  function print(data){
    console.log(data);
  }

  function error403(data){
    console.log(data);
  }

    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    // btn.onclick = function() {
    //     modal.style.display = "block";
    // }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }