from django.apps import AppConfig


class AppProfilConfig(AppConfig):
    name = 'app_profil'
