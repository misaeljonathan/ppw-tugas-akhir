	// Setup an event listener to make an API call once auth is complete
  function onLinkedInLoad() {
      IN.Event.on(IN, "auth", sharedContent);
  }

  // Handle the successful return from the API call
  function onSuccess(data) {
    console.log(data);
  }

  // Handle an error response from the API call
  function onError(error) {
    console.log(error);
  }

  function closeSession(){
    sessionStorage.clear();
    IN.User.logout();
    document.getElementById("login-text").innerHTML = "sign in";
  }


  function sharedContent() {
    document.getElementById("login-text").innerHTML = "sign out";
    counter=1;
    console.log("Success"+IN.User.isAuthorized());
    IN.API.Raw("/companies?format=json&is-company-admin=true").result(
      function (user_data) {
        company_data = user_data.values[0];
        comp_id = company_data.id;
        IN.API.Raw(/companies/+comp_id+":(id,name,logo-url,specialties,website-url,description)?format=json").result(saveSession);
      }
      ).error(error403);
  }

  function saveSession(data){
    sessionStorage.setItem('company', JSON.stringify(data));
    company_id = JSON.parse(sessionStorage.getItem('company')).id;
    console.log(company_id);
  }

  function error403(data){
    console.log(data);
  }

    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("login-btn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    var counter = 0;
    btn.onclick = function() {
        if(counter%2===0){
          modal.style.display = "block";
        }else{
          closeSession();
          counter=0;
        }
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }

    //DIBAWAH INI APP COMMENT :)

    $("#commentButton").click(function () {
      inputText = $("#textBoxNya").val();
      console.log( $(this).val() );
      console.log("test");
      $.ajax({
          method: "POST",
          url: '{% url "app_homepage:add_comment" %}',
          data: {
              'comment' : inputText
          },
          dataType: 'json',
          success: function (data) {
              data = JSON.parse(data);
              html = '<div class="comment">' +
                "<h1>" + data.comment +  " HEHEHE POSTNYA KELUAR JUGA " +
                    '</h1>'+ "</div>";
              console.log(data);
              $("#postnya").append(html);
          }
      });
  });
