from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini'
    }
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':' Komentar Anda ...'
    }

    status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
