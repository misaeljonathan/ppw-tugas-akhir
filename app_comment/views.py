from django.shortcuts import render, redirect, get_object_or_404
from datetime import datetime, date
from .models import Update, Comment
from django.http import HttpResponseRedirect
from .forms import Message_Form, Comment_Form
from django.views.decorators.csrf import csrf_exempt

response = {'author': "Misael Jonathan"}
curr_year = int(datetime.now().strftime("%Y"))
timeline_status = {}
def index(request):
    response = {}
    response['message_form'] = Message_Form
    message = Update.objects.all()
    response['message'] = message
    comment = Comment.objects.all()
    response['comment'] = comment
    response['todo_form'] = Message_Form
    response['comment_form'] = Comment_Form
    return render(request, 'app_comment.html', response)

def status_post(request):
    form = Message_Form(request.POST or None)
    response['message_form'] = Message_Form
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        message = Update(message=response['status'])
        message.save()
        return HttpResponseRedirect('/app_comment/')
    else:
        return HttpResponseRedirect('/app_comment/')

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

def add_comment(request, pk):
	status = Update.objects.get(pk=pk)
	form = Comment_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['comment'] = request.POST['comment']
		comment=Comment(comment=response['comment'])
		comment.status = status
		comment.save()
		return redirect('/app_comment/')
	else:
		return HttpResponseRedirect('/app_comment/')
